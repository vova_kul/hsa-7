# HSA-7 Homework project for web servers

This an HSA-7 Homework project to show caching on web servers.

## Preparation

```
docker-compose up
```

## Verify caching is workind

#### First request cache miss

![First request](./screens/first_request.png?raw=true "First request")

#### Third request cache hit

![Third request](./screens/third_request.png?raw=true "Third request")

#### The cache file is created

![The cache file](./screens/cache_file.png?raw=true "The cache file")

#### Send request to remove a cache

![Clear cache](./screens/clear-cache.png?raw=true "Clear cache")

#### The cache file is removed and cache miss

![Removed cache file](./screens/cleared-cache-file.png?raw=true "Removed cache file")

![Cache miss](./screens/cache-miss.png?raw=true "Cache miss")
